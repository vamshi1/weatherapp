import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-cache',
  templateUrl: './cache.component.html',
  styleUrls: ['./cache.component.css']
})
export class CacheComponent implements OnInit {
 
  //getting data from parent
  @Input() recents: Object

  constructor() { }

  ngOnInit(): void {
  }

}
