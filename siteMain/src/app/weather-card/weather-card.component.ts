import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../weather.service';

@Component({
  selector: 'app-weather-card',
  templateUrl: './weather-card.component.html',
  styleUrls: ['./weather-card.component.css']
})
export class WeatherCardComponent implements OnInit {

  weatherData: any

  constructor(private service: WeatherService) { }

  ngOnInit(): void {

    //Instead getting data from parent using subject for state management
    this.service.weatherObservable.subscribe((data) => {
      this.weatherData = data;
    })
  }

}
