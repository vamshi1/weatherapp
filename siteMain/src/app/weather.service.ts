import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  //server url
  API_URL = 'http://localhost:3000';

  //state mangement
  private weatherSubject = new BehaviorSubject('');
  weatherObservable = this.weatherSubject.asObservable();

  constructor(private http: HttpClient) {
  }

  getWeather(city): Observable<any> {

    //return full response object along with status code
    return this.http.get(`${this.API_URL}/weather?city=${city}`, { observe: 'response' });
  }

  changeSubjectData(data) {

    //update the subject with data
    this.weatherSubject.next(data);
  }
}
