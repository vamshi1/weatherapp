import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../weather.service';


@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {

  city: any
  showSpinner: Boolean
  showWeatherCard: Boolean
  cache: any

  constructor(private service: WeatherService) { }

  ngOnInit(): void {
    this.showSpinner = false;
    this.showWeatherCard = false;
    
    //As there is only one route maintaining cache without session storage
    this.cache = [];
  }

  searchCity() {
    this.showSpinner = true;
    this.showWeatherCard = false;

    //first check if it is present in cache
    let findInCache = this.cache.findIndex((item) => {
      if (item && item.name.toLowerCase() == this.city.toLowerCase()) {
        return item;
      }
    });

    //if found, update subject with latest data
    if (findInCache >= 0) {
      this.service.changeSubjectData(this.cache[findInCache]);
      this.showSpinner = false;
      this.showWeatherCard = true;
    }
    else {

      //else call server to get data
      this.service.getWeather(this.city.toLowerCase()).subscribe((data) => {
        let obj = {
          status: data.status,
          name: this.city.toLowerCase(),
          data: data.body.data
        }

        //updatesubject
        this.service.changeSubjectData(obj);

        this.showSpinner = false;
        this.showWeatherCard = true;

        //add item to cache
        this.cache.push(obj)
      }, (error) => {

        //if city not found, we get 404 status 
        this.service.changeSubjectData({
          status: error.status,
          name: this.city,
          data: error.error
        });
        this.showSpinner = false;
        this.showWeatherCard = true;
      })
    }
  }

}
