# Weather App #

This app gives the weather of your city.

* serverApi contains backend Node server code.
* siteMain contains frondend Angular code.

# Assumptions #

* On the server side, as weather changes everyday, the old cached data in the database is deleted asynchronously. When server recieves any request it first checks in database for that city.If it is present , the data from database is returned else we call openweather Api and returns data.

* On the client side , as there is only one route using an array instead of sessionStorage to cache the data . So if the user tries to search for a particular city, first it checks in cache , if it is not present in cache then it calls server to get the data.Here I'm using Behaviour subject for state management. And the data in cache displays under 'Recents' section.